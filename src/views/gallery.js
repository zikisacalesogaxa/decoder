import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Image } from 'cloudinary-react';

export class gallery extends Component {
    state = {
        images: null,
        cloudName: "dhkevzbza"
    }

    componentDidMount() {
        document.querySelectorAll('.video-bg').forEach(elem => {
            elem.style.filter = "blur(8px)";
        });
    }

    componentWillMount() {
        let collection = [];

        for (let i = 1; i < 7; i++) {
            const image = i;
            collection.push(<Image key={i} publicId={"gallery/" + image} cloudName={this.state.cloudName} />)
        }

        this.setState({
            images: collection
        });
    }

    render() {
        const { images } = this.state;
        return (
            <main className="gallery">
                <section className="collection">
                    {
                        images.map((image, index) => {
                            return (image)
                        })
                    }
                </section>
                <footer className="nav-footer">
                    <ul>
                        <NavLink to="/">
                            <li>INITIATE</li>
                        </NavLink>
                        <NavLink to="/music">
                            <li>MUSIC</li>
                        </NavLink>
                    </ul>
                </footer>
            </main>
        )
    }
}

export default gallery;
