import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export class homepage extends Component {
    componentDidMount() {
        document.querySelectorAll('.video-bg').forEach(elem => {
            elem.style.filter = "blur(0px)";
        });
    }
    render() {
        return (
            <main className="homepage">
                <main className="main-content">
                    <img className="logo" src="/assets/logo.svg" alt="" />
                    <aside>
                        <a href="/">
                            <img src="/assets/icons/spotify.png" alt="" />
                        </a>
                        <a href="/">
                            <img src="/assets/icons/instagram.png" alt="" />
                        </a>
                        <a href="/">
                            <img src="/assets/icons/facebook.png" alt="" />
                        </a>
                        <a href="/">
                            <img src="/assets/icons/soundcloud.png" alt="" />
                        </a>
                    </aside>
                </main>
                <footer className="nav-footer">
                    <ul>
                        <NavLink to="/gallery">
                            <li>GALLERY</li>
                        </NavLink>
                        <NavLink to="/music">
                            <li>MUSIC</li>
                        </NavLink>
                    </ul>
                </footer>
            </main>
        )
    }
}

export default homepage
