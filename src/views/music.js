import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export class music extends Component {
    componentDidMount() {
        document.querySelectorAll('.video-bg').forEach(elem => {
            elem.style.filter = "blur(8px)";
        });
    }
    render() {
        return (
            <main className="music">
                <section className="songs">
                    <div className="song">
                        <div className="poster">
                            <img src="/assets/disco-photo-1.png" alt="" />
                        </div>
                        <div className="details">
                            <h3>&#47;&#47; 001</h3>
                            <h2>MOU5TRAP</h2>
                            <h1>DANGER</h1>
                            <button>BUY NOW</button>
                        </div>
                    </div>
                    <div className="song">
                        <div className="poster">
                            <img src="/assets/disco-photo-2.png" alt="" />
                        </div>
                        <div className="details">
                            <h2>&#47;&#47; 002</h2>
                            <h2>MOU5TRAP</h2>
                            <h1>DANGER</h1>
                            <button>BUY NOW</button>
                        </div>
                    </div>
                    <div className="song">
                        <div className="poster">
                            <img src="/assets/disco-photo-3.png" alt="" />
                        </div>
                        <div className="details">
                            <h2>&#47;&#47; 003</h2>
                            <h2>MOU5TRAP</h2>
                            <h1>DANGER</h1>
                            <button>BUY NOW</button>
                        </div>
                    </div>
                    <div className="song">
                        <div className="poster">
                            <img src="/assets/disco-photo-4.png" alt="" />
                        </div>
                        <div className="details">
                            <h2>&#47;&#47; 004</h2>
                            <h2>MOU5TRAP</h2>
                            <h1>DANGER</h1>
                            <button>BUY NOW</button>
                        </div>
                    </div>
                    <div className="song">
                        <div className="poster">
                            <img src="/assets/disco-photo-5.png" alt="" />
                        </div>
                        <div className="details">
                            <h2>&#47;&#47; 005</h2>
                            <h2>MOU5TRAP</h2>
                            <h1>DANGER</h1>
                            <button>BUY NOW</button>
                        </div>
                    </div>
                </section>
                <footer className="nav-footer">
                    <ul>
                        <NavLink to="/">
                            <li>INITIATE</li>
                        </NavLink>
                        <NavLink to="/gallery">
                            <li>GALLERY</li>
                        </NavLink>
                    </ul>
                </footer>
            </main>
        )
    }
}

export default music
