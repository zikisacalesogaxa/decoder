import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Video } from 'cloudinary-react';

// pages
import homepage from '../../views/homepage';
import gallery from '../../views/gallery';
import music from '../../views/music';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      images: ["livesong"],
      cloudName: "dhkevzbza",
      controlButton: "pause.svg"
    }
  }


  targetElement = null;

  componentDidMount() {
    let video = document.getElementById('main-song');
    video.play();
  }

  playPause = () => {
    let video = document.getElementById('main-song');
    if (video.paused) {
      this.setState({
        controlButton: "pause.svg"
      });
      video.play();
    } else {
      video.pause();
      this.setState({
        controlButton: "play.svg",
      });
    }
  }

  render() {
    const { controlButton } = this.state;
    return (
      <Router>
        <Video id="main-song" cloudName={this.state.cloudName} publicId={this.state.images[0]} controls={false} loop={true} autoPlay={true}></Video>
        <button id="main-song-controller" onClick={this.playPause}>
          <img src={ "/assets/icons/" + controlButton } alt="" />
        </button>
        <video className="mobile-vid video-bg" autoPlay loop muted poster="/assets/bg/poster.png">
          <source src="assets/videos/mobile.mp4" />
        </video>
        <video className="desktop-vid video-bg" autoPlay loop muted poster="/assets/bg/poster.png">
          <source src="assets/videos/desktop.mp4" />
        </video>
        <div>
          <Route path="/" exact component={homepage} />
          <Route path="/gallery" exact component={gallery} />
          <Route path="/music" exact component={music} />
        </div>
      </Router>
    );
  }
}

export default App;
